# LOG8415-TP3

How to fork this repository?
Please follow the instructions here: https://help.github.com/articles/fork-a-repo/

Once you forked thi repository, create a folder with a specific name for your group. Example: "tigers_2019_tp2"

Before the deadline, when you are ready to submit your work, make a pull request: https://help.github.com/articles/creating-a-pull-request-from-a-fork/

Questions? Send me an email: a.abtahizadeh[at]polymtl[dot]ca