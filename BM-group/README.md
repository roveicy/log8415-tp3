In order to run the experiment, Install Client.py and runner.sh on the client. 

Gate_keeper.py should be run in the gate keeper server
Server.py sould be run on the master node

===========================================================
Content
============================================================

==============
Scripts folder
============== 

contain all the scripts used in this TP
1. Client.py....Client script
2. runner.sh-----Shell script to run the client five times
3. Gate_keeper.py....Python script to be run on the gate keeper
4. Server.py....Python script that is run on the master node
5. Token_Generator.py....python script that is used to generate the security tokens for the clients

=================
Running sequence
===================

1. Run Server.py
2. Run Gate_keeper.py if needed
3. Run Client.py


=====================
Results folder
====================

This folder contains all results in our experiement

======================
cluster_config_files
====================

This folder contains two configuration files. One for the master node and the other is for data nodes.

=========================
sysbench-instructions.txt
=========================

This file contains sysbench running instructions we followed to compare performance between cluster and stand alone



=================================================|| =========================================
