import socket
from _thread import *
import random
import string

# ===========================
# configuration
# ==========================

port = 21712  # port to connect with clients
trusted_host_ip = '172.31.90.161'
host = ''
trusted_host_port = 21719
# IP address of other db server
# sl1: '172.31.85.11'
# sl2:'172.31.81.44'
# l3:'172.31.89.214'

list_of_valid_tokens=['x8EdhPNaf2v9j7vYy9ngsvqfocCYJndoHyCeIE6MAIU'
'tODb-A1loABIHXeOjuKj4Xb2ri3rbXYpHpIOMWn6mJU',
'5yZCyM9QhJg7OIoCkJccOUcM8tT8yIBB1ylgOGXfkIk',
'vvba-rT6d0lYd1OWpifKl_XWcO2zdpnI6ZgOJ9G59R0'] #list of tokens allowed by the gate keeper

list_of_denied_commands= ['ALTER', 'DROP', 'RENAME', 'MODIFY', 'UPDATE', 'DELETE','GRANT', 'REVOKE','ADD','CREATE','SET','REPLACE','DATABASE'] #list of non allowed database operations

#function that checks invalid query
def invalid_query_checker(query):
    upper_case = query.upper()
    if any(word in upper_case for word in list_of_denied_commands):
        return False, "Illegal query, Your query is denied"
    else:
        return True, query

#function to split the reqests in to token and query
def split_token_query(request):
    splits = request.split(':')
    return splits


#function to check if the security token is valid or not

def check_token(token, query):
    try:
        index = list_of_valid_tokens.index(token)
        return invalid_query_checker(query)
    except ValueError as e:
        return False, "Invalid security token"

#thsi function is called to evaluate incoming request. All of the above functions are called inside this function

def is_request_valid(request):
    splits = split_token_query(request)
    if len(splits) != 2:
        return False, "Request is malformed or it does not contain security token"
    else:
        return check_token(splits[0], splits[1])




def client_thread(conn, ths):
    try:
        while True:
            data = conn.recv(65536)

            # do some processing of the received request
            request = data.decode('utf-8')
            print("processing request...")
            if not data:
                break
            # do some filtering logic for the request
            # and forward the request to trusted HOST, valid is a boolean that tells if the request is valid and msg is either the query for valid requests or error message for invalid requests
            valid, msg = is_request_valid(request)
            if not valid:
                conn.sendall(msg.encode())
                print(msg)
            else:
                ths.send(request.encode())
            # fetch response from trusted host
                response = ths.recv(65536)
            # forward response from trusted host to clients
                conn.sendall(response)
    except socket.error as e:
        print(str(e))

def main():

     # create a client connection socket
    ss= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #create another socket for trusted host (ths)
    ths= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ths.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    try:
        ths.connect((trusted_host_ip, trusted_host_port))  # open separate connection to the trusted host
        print("Gate keepr is connected to  trusted host on port: " + str(trusted_host_port))
        ss.bind((host, port))  # Open a port and bind this port to any of the incomming connections
        print("Server ready to accept connections from clients on port: " + str(port))
    except socket.error as e:
        print(str(e))
    
    ss.listen(5)

    try:
        while True:
            # accept connections from outside
            conn, addr = ss.accept()
            print("Connected to: " + str(addr) + "\n")
            # now start new thread for each request
            start_new_thread(client_thread, (conn,ths))
    except KeyboardInterrupt:
        print("Server is stopped by user, closing socket..")
        ss.close()


if __name__ == "__main__":
    main()
