import socket
from _thread import *
import random
import string
from ping3 import ping #library to implement ping based pattern



import pymysql
# ===========================
# configuration
# ==========================
db_servers=[]
host = ''  # server host used for direct hit
port = 21719  # port to connect with clients
dbhost = 'localhost' # We use this for direct hit
dbuser = 'mb'
dbpwd = ''
dbname = 'sakila'

# IP address of other db server
# sl1: '172.31.85.11'
# sl2:'172.31.81.44'
# l3:'172.31.89.214'


def connect_mysql(host):
    try:
        connection=pymysql.connect(host,dbuser,dbpwd, dbname) #connect to mysql database
        cursor=connection.cursor()
        return cursor, connection
    except Exception as ex:
        print("Exception: ",ex)
        quit()


#fucntion to execute any query on the opened database connection
def execute_query(cursor, query, conn):
    try:
        cursor.execute(query)
        res=cursor.fetchall()
        return str(res)
    except Exception as ex:
        #print("Exception: ",ex)
        return str(ex)

#fucntion to close opened database connection
def close_connection(conn):
    conn.close()
    print("Database connection is closed")





# define client thread as a fucntion that runs the server logic for each client request
def client_thread(conn):
    try:
        while True:
            data = conn.recv(65536)
            # do some processing of the received request
            request = data.decode('utf-8')
            if not data:
                break
	    #Comment one of the following sharding approach at a time for testing, currently the pattern is sharding by range
            #response = proxy_direct_hit(request)
            #response= proxy_random_hit(request)
            #response=proxy_pingbased_hit(request)
            #response= sharding_by_hash(request)
            response= sharding_by_range(request)
            conn.sendall(str.encode(response))
    except socket.error as e:
        print(str(e))

# direct hit pattern, we need to limit the result set to 15 because the buffer size of our socket can not handle more data than thsi

def proxy_direct_hit(request):
    cursor, connection = connect_mysql(dbhost)
    request= request.replace(';', '')
    request+= " LIMIT 15;"
    #print(request)
    res = execute_query(cursor, request, connection)
    close_connection(connection)
    #print(res)
    return res

# Random hit, choose randomly among slave nodes


def proxy_random_hit(request):
    index = random.randint(0, 2) #generate randomly between 0, 1, 2
    cursor, connection = connect_mysql(db_servers[index])
    request= request.replace(';', '') #this is needed to append the limit to users query
    request+= " LIMIT 15;"
    res = execute_query(cursor, request, connection)
    close_connection(connection)
    return db_servers[index]+ " "+ res

# proxy custom hit based on ping value

def proxy_pingbased_hit(request):
    lowest = 233434343 # randm large number for initalization
    winner_server = '' # index value with smalles delay
    for server in db_servers:
        delay = ping(server, unit='ms', timeout=4) #this function returns the delay of a ping operation in ms
        if delay < lowest:
            lowest = delay
            winner_server = server #the server with the lowest delay
    cursor, connection = connect_mysql(winner_server)
    request= request.replace(';', '')
    request+= " LIMIT 15;"
    res = execute_query(cursor, request, connection)
    close_connection(connection)
    return winner_server+" : "+ res

# read sharding by range
def sharding_by_range(request):
    # add a code that filters a request based on date
    #sharding works by day:
    #sl1: 1 to 10
    #sl2: 11 to 20
    #sl3: 21 to 30
    #remove semicolun if there is in the query_
    request= request.replace(';', '')
    final_result= ""
    query_uppend_server1= ' AND DAY(rental.return_date) BETWEEN 1 AND 10 LIMIT 5;' #get between 1 and 10
    query_uppend_server2= ' AND DAY(rental.return_date) BETWEEN 11 AND 20 LIMIT 5;' #get between 11 and 20
    query_uppend_server3= ' AND DAY(rental.return_date) BETWEEN 21 AND 31 LIMIT 5;' #get between 21 and 31
    #now send the corresponding query for each server
    #server 1
    cursor, connection = connect_mysql(db_servers[0])
    final_result+= "<"+db_servers[0]+">" + execute_query(cursor, request+query_uppend_server1, connection)
    close_connection(connection)

    #server 2
    cursor, connection = connect_mysql(db_servers[1])
    final_result+= "<"+db_servers[1]+">" + execute_query(cursor, request+query_uppend_server2, connection)
    close_connection(connection)

    #server 3
    cursor, connection = connect_mysql(db_servers[2])
    final_result+= "<"+db_servers[2]+">" + execute_query(cursor, request+query_uppend_server3, connection)
    close_connection(connection)

    return final_result

# read sharding by hash
def sharding_by_hash(request):
    # hash the whole query  plus some randomeness for balancing and compute the modulo 3
    index = hash(request.join(random.choices(string.ascii_uppercase + string.digits, k=6))) % 3
    cursor, connection = connect_mysql(db_servers[index])
    request= request.replace(';', '')
    request+= " LIMIT 15;"
    res = execute_query(cursor, request, connection)
    close_connection(connection)
    return db_servers[index] + " : " + res

def main():
    # iniitalize db_server slave nodes list
    db_servers.append('172.31.85.11')
    db_servers.append('172.31.81.44')
    db_servers.append('172.31.89.214')
   
    # create an INET, STREAMing socket
    ss= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # bind the socket to a public host, and a well-known port
    try:
        ss.bind((host, port))  # Open a port and bind this port to any of the incomming connections
        print("Server ready to accept connections on port: " + str(port))
    except socket.error as e:
        print(str(e))
  
    ss.listen(5)

    try:
        while True:
            # accept connections from outside
            conn, addr = ss.accept()
            print("Connected to: " + str(addr) + "\n")
            # now start new thread for each client
            start_new_thread(client_thread, (conn,))
    except KeyboardInterrupt:
        print("Server is stopped by user, closing socket..")
        ss.close()


if __name__ == "__main__":
    main()
