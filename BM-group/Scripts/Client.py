# ======================
# This is our client application for all tests

import socket
import threading
import time
import random
# importing statistics module
import statistics

# connection information
# trusted host public ip, 
#HOST= '35.174.104.204'
#PORT=21719

#GATE KEEPER infomration comment the following two lines to run with out gate keepr and uncomment the above two lines (truste host Ip and port)
HOST = '3.93.6.51'
PORT = 21712
response_times=list() #glaobal variable to store response time

#token = '5yZCyM9QhJg7OIoCkJccOUcM8tT8yIBB1ylgOGXfkIk'
token = '5yZCyM9QhJg7OIoCkJccOUcM8tT8yIBB1ylgOGXfkIk' #token of our client instance
request_query = "SELECT customer.first_name, customer.last_name, rental.return_date, film.title FROM customer INNER JOIN rental ON customer.customer_id= rental.customer_id INNER JOIN inventory ON rental.inventory_id=inventory.inventory_id INNER JOIN film ON inventory.film_id= film.film_id WHERE MONTH(rental.return_date) = 5;" #query used in all requests
#function to implement senario

def execute_senario(conn, request):
    print("Please wait.. Senario is executing...")
    conn_setup_time=0.0;
    try:
        t1=time.time()
        conn.connect((HOST, PORT))  # open separate connection to the Server
        t2=time.time()
        conn_setup_time=t2-t1
        senario_rt=[]
        for j in range(1,4):
            req_rt=[]
            for i in range(1,51):
                t1=time.time()
                conn.send(request.encode())  # Send encoded request to the Server
                data = conn.recv(65536)
                t2=time.time()
                req_rt.append(t2-t1)
                
            senario_rt.append(statistics.median(req_rt))
            index = random.randint(1, 3) #generate random sleep time between 1 and 3 seconds
            time.sleep(index)

        conn.close()
        med_response_time_all_senarios = statistics.median(senario_rt)
        
        final_med_response_time_ms = (conn_setup_time + med_response_time_all_senarios) * 1000 #response time in milisseconds
        print ("Median response time: "+ str(final_med_response_time_ms) + " ms")
        return ""
    except socket.error as e:
        return str(e)
    finally:

        return "Senario finished"

# client function to be run in a new thread this is important to simulate multiple clients


def client_runner(id):
    # first connect to Server
    # crete a socket object
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#CONCAT(customer.first_name, " ", customer.last_name) AS full_name ,
#append the request to the client token
    sec_request= token + ":" + request_query
   #sec_request= request_query #uncomment this line when you do not use gate keeper
    execute_senario(conn, sec_request)




def main():
    t = threading.Thread(target= client_runner, name='Thread ' + str(1), args=("Thread " + str(1), ))
    t.start()
    t.join()





if __name__ == "__main__":
    main()
